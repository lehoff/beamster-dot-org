defmodule Die do

  def roll(n \\ 6) do
    :random.uniform(n)
  end
  
  def roll_2d6() do
    {roll(), roll()}
  end

  def sum_2d6({x, y}) do
    x+y
  end
  
end
