defmodule Coin do

  def toss() do
    if :random.uniform(2) == 1 do
      :heads
    else
      :tails
    end
  end

end
