defmodule Stats do

  def sum([]) do
    0
  end

  def sum([h|t]) do
    h + sum(t)
  end

  def sample(f, 0) do
    []
  end

  def sample(f, n) do
    [f.()|sample(f, n-1)]
  end

  def max_list([]) do
    :undefined
  end

  def max_list([x]) do
    x
  end

  def max_list([h|t]) do
    max(h, max_list(t))
  end

  def member?(_, []) do
    False
  end

  def member?(x, [x|_]) do
    True
  end

  def member?(x, [_,t]) do
    member?(x, t)
  end

  def count(_, []) do
    0
  end

  def count(x, [x|xs]) do
    1 + count(x, xs)
  end

  def count(x, [_|xs]) do
    count(x, xs)
  end
    
end
