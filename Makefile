
SUBDIRS := jb

# each module will add to this
SRC :=
# include the description for
#   each subdir
include $(patsubst %,\
    %/module.mk,$(SUBDIRS))
# determine the object files
OBJ :=                    \
	$(patsubst %.asciidoc,%.html,     \
	$(filter %.asciidoc,$(SRC))) 


build: ${OBJ} index.html
	asciidoc -b html5 -a icons -a toc2 -a theme=flask index.asciidoc


%.html: %.asciidoc
	asciidoc -b html5 -a icons -a theme=flask $<
