 defmodule Yatzy.Score do


  @doc ~S"""
  upper(n, ds) should return the score for the roll ds used in the slot for n number
  of eyes in the upper section of the score sheet.

  ## Examples
     iex> Yatzy.Score.upper(1, [2,3,1,2,1])
     2
     iex> Yatzy.Score.upper(2, [2,2,2,3,2])
     8
  """
  def upper(n, ds) do
    n * Stats.count(n, ds)
  end

  @doc ~S"""
  chance(ds) should return the sum of the eyes on the dice.

  ## Examples
     iex> Yatzy.Score.chance([3,4,2,4,1])
     14
  """
  def chance(ds) do
    Stats.sum(ds)
  end

  @doc ~S"""
  yatzy(ds) should return 50 if all dice are showing the same number of eyes.

  ## Examples
     iex> Yatzy.Score.yatzy([2,3,3,3,3])
     0
     iex> Yatzy.Score.yatzy([4,4,4,4,4])
     50
  """
  def yatzy([h,h,h,h,h]) do
    50
  end

  def yatzy(_) do
    0
  end

  @doc ~S"""
  small_straight(ds) should return 15 for any set of dice containing the
  sequence 1-2-3-4-5 in any order.

  ## Examples
     iex> Yatzy.Score.small_straight([1,2,3,4,6])
     0
     iex> Yatzy.Score.small_straight([2,4,3,1,5])
     15
  """
  def small_straight(ds) do
    case Enum.sort(ds) do
      [1,2,3,4,5] -> 15
      _ -> 0
    end
  end

  @doc ~S"""
  large_straight(ds) should return 20 for any set of dice containing the
  sequence 2-3-4-5-6 in any order.

  ## Examples
     iex> Yatzy.Score.large_straight([5,2,3,2,6])
     0
     iex> Yatzy.Score.large_straight([6,2,4,3,5])
     20
  """
  def large_straight(ds) do
    case Enum.sort(ds) do
      [2,3,4,5,6] -> 20
      _ -> 0
    end
  end

  @doc ~S"""
  four_of_a_kind(ds) has a value when there are four of a kind in the roll and then
  its value is the total number of eyes on the dice being four of a kind.

  ## Examples
     iex> Yatzy.Score.four_of_a_kind([2,3,3,3,5])
     0
     iex> Yatzy.Score.four_of_a_kind([5,5,5,2,5])
     20
  """
  def four_of_a_kind(ds) do
    case Enum.sort(ds) do
      [_,a,a,a,a] -> 4*a
      [a,a,a,a,_] -> 4*a
      _ -> 0
    end
  end

  @doc ~S"""
  three_of_a_kind(ds) has a value when there are three of a kind in the roll and then
  its value is the total number of eyes on the dice being three of a kind.

  ## Examples
     iex> Yatzy.Score.three_of_a_kind([2,3,4,3,5])
     0
     iex> Yatzy.Score.three_of_a_kind([4,1,4,2,4])
     12
  """
  def three_of_a_kind(ds) do
    case Enum.sort(ds) do
      [_,_,a,a,a] -> 3*a
      [_,a,a,a,_] -> 3*a
      [a,a,a,_,_] -> 3*a
      _ -> 0
    end
  end

  @doc ~S"""
  one_pair(ds) has a value when there is a pair in the roll and then
  its value is the total number of eyes on the dice in the pair.

  ## Examples
     iex> Yatzy.Score.one_pair([2,3,4,1,5])
     0
     iex> Yatzy.Score.one_pair([4,6,6,2,5])
     12
  """
  def one_pair(ds) do
    case Enum.sort(ds) do
      [_,_,_,a,a] -> 2*a
      [_,_,a,a,_] -> 2*a
      [_,a,a,_,_] -> 2*a
      [a,a,_,_,_] -> 2*a
      _ -> 0
    end
  end

  @doc ~S"""
  two_pairs(ds) has a value when there is two pairs in the roll and then
  its value is the total number of eyes on the dice in the two pairs.

  ## Examples
     iex> Yatzy.Score.two_pairs([2,3,3,3,3])
     0
     iex> Yatzy.Score.two_pairs([2,4,6,2,6])
     16
  """
  def two_pairs(ds) do
    case Enum.sort(ds) do
      [_,a,a,b,b] when a != b -> 2*a + 2*b
      [a,a,_,b,b] when a != b -> 2*a + 2*b
      [a,a,b,b,_] when a != b -> 2*a + 2*b
      _ -> 0
    end
  end

  @doc ~S"""
  full_house(ds) has a value when there is three of a kind and a pair in the roll.
  The value is the sum of the eyes on the dice.

  ## Examples
     iex> Yatzy.Score.full_house([3,3,3,3,3])
     0
     iex> Yatzy.Score.full_house([3,3,6,3,6])
     21
  """
  def full_house(ds) do
    case Enum.sort(ds) do
      [a,a,b,b,b] when a !=b -> 2*a + 3*b
      [a,a,a,b,b] when a !=b -> 3*a + 2*b
      _ -> 0
    end
  end

end
